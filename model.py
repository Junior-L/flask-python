from exts import db

class EntityBase(object):
    def to_json(self):
        fields = self.__dict__
        if "_sa_instance_state" in fields:
            del fields["_sa_instance_state"]
        return fields

class Server(db.Model, EntityBase):
    #数据表明、字段
    __tablename__ = 'serverconfig'
    id = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.String(20))
    port = db.Column(db.String(20))
    user = db.Column(db.String(100))
    password = db.Column(db.String(100))

class AdminUser(db.Model, EntityBase):
    __tablename__ = 'admin'
    id = db.Column(db.Integer, primary_key=True)
    admin = db.Column(db.String(20))

class KouLing(db.Model, EntityBase):
    __tablename__ = 'kouling'
    id = db.Column(db.Integer, primary_key=True)
    kouling = db.Column(db.String(20))

class Computer(db.Model, EntityBase):
    __tablename__ = 'computer'
    id = db.Column(db.Integer, primary_key=True)
    ip = db.Column(db.String(20))


